# Doctests
"""
  >>> line1 = Line(Point(1, -1), Point(5, 3))
  >>> line2 = Line(Point(2, 4), Point(4, -2))
  >>> print(line1.intersects(line2))
  (3.0, 1.0)
"""

def runtests():
    import doctest
    doctest.testmod()
