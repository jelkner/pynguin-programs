class Line:
    def __init__(self, p1, p2):
        self.p1 = p1
        self.p2 = p2
        dy = p1.y - p2.y
        dx = p1.x - p2.x
        self.m = None if dx == 0 else dy / dx
        self.b = None if self.m == None else p1.y - (self.m * p1.x)

    def y_given_x(self, x):
        if self.m == None:
            return None
        return self.m * x + self.b

    def x_given_y(self, y):
        if self.m == 0:
            return None
        return (y - self.b) / self.m

    def intersects(self, other):
        if self.m == None and other.m == None:
            if self.p1.x == other.p1.x:
                return self
            else:
                return None
        if self.m == 0 and other.m == 0:
            if self.p1.y == other.p1.y:
                return self
            else:
                return None
        x = (other.b - self.b) / (self.m - other.m)
        y = self.m * x + self.b
        return Point(x, y)

    def draw_segment(self):
        x, y, heading = xyh()
        goto(self.p1.x, self.p1.y)
        self.p1.draw()
        lineto(self.p2.x, self.p2.y)
        self.p2.draw()
        goto(x, y)
        turnto(heading)
