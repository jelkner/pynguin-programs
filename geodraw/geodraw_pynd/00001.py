class Point:
    def __init__(self, x, y, name=""):
        self.x = x
        self.y = y
        self.name = name

    def __str__(self):
        return "{0}({1}, {2})".format(self.name, self.x, self.y)

    def inview(self):
        xmin, ymin, xmax, ymax = viewcoords()
        return xmin <= self.x <= xmax and ymin <= self.y <= ymax

    def draw(self):
        x, y, heading = xyh()
        goto(self.x, self.y)
        fill(color=(0, 0, 0))
        circle(5, center=True)
        nofill()
        goto(self.x - 7, self.y - 25)
        turnto(0)
        write(self.name)
        goto(x, y)
        turnto(heading)
