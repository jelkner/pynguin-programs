mode('turtle')

def draw_arrow():
    x, y, heading = xyh()
    penup()
    left(90)
    backward(5)
    fill(color=(0, 0, 0))
    pendown()
    forward(10)
    right(104)
    forward(20.615528)
    right(152)
    forward(20.615528)
    nofill()
    goto(x, y)
    turnto(heading)
