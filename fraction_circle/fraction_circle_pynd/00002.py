def fraction_circle(n, d, radius=300):
    pos = xy()
    slice = 360 / d
    shade(n * slice, radius)
    xy(*pos)
    turnto(270)
    color(0, 0, 0)
    width(3)
    nofill()
    circle(radius, True)
    for count in range(d):
        forward(radius)
        penup()
        backward(radius)
        right(slice)
        pendown()
