def fraction_bar(num, denom):
    goto(-500, -50)
    partsize = 1000 / denom
    shadesize = num * partsize
    color(255, 127, 0)
    fill(color=(255, 127, 0))
    for i in 1, 2:
        forward(shadesize)
        right(90)
        forward(100)
        right(90)
    nofill()
    color(0, 0, 0)
    width(4)
    for seg in range(denom):
        for i in 1, 2:
            forward(partsize)
            right(90)
            forward(100)
            right(90)
        forward(partsize)
