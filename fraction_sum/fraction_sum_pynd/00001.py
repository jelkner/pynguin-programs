def fraction_circle(n, d, radius):
    pos = xy()
    slice_size = 360 / d
    filled = n
    not_filled = d - n
    xy(*pos)
    turnto(270)
    color(0, 0, 0)
    width(3)
    for slice in range(filled):
        fill()
        arc(radius, slice_size, center=True, pie=True)
    for slice in range(not_filled):
        nofill()
        arc(radius, slice_size, center=True, pie=True)
