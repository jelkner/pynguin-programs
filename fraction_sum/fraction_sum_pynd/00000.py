from math import *


def run():
    reset()
    mode('turtle')
    bgcolor(255, 255, 255)
    speed('medium')
    fraction_sum(3, 4, 3, 7)
    avatar('hidden')
