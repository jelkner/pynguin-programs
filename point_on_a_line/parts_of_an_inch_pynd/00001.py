def draw_segment(start, end, parts=16):
    part_size = 1000/parts
    xpos = -500
    goto(xpos, 0)
    color(0, 0, 0)
    width(3)
    forward(1000)
    turnto(270)
    for part in range(parts + 1):
        goto(xpos, 0)
        if part % 8 == 0:
            forward(50)
        elif part % 4 == 0:
            forward(40)
        elif part % 2 == 0:
            forward(30)
        else:
            forward(20)
        xpos += part_size
    # label ends
    width(1)
    turnto(0)
    goto(-505, 15)
    write(start)
    goto(495, 20)
    write(end)
