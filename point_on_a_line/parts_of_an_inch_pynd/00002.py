def place_point(n, d):
    xpos = -500 + (1000 * n / d)
    goto(xpos, 0)
    color(255, 0, 0)
    fill(color=(255, 0, 0))
    circle(10, True)
