def draw_segment(start, end, parts):
    part_size = 1000/parts
    xpos = -500
    goto(xpos, 0)
    color(0, 0, 0)
    width(3)
    forward(1000)
    turnto(270)
    for part in range(parts + 1):
        goto(xpos, 20)
        forward(40)
        xpos += part_size
    # label ends
    width(1)
    turnto(0)
    goto(-505, 50)
    write(start)
    goto(495, 50)
    write(end)
